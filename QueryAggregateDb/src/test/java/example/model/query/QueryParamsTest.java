package example.model.query;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class QueryParamsTest {

  @Test
  void of() {}

  @Test
  void testIsWithinTransactionCountBoundry() {
    var param = new QueryParams();
    param.setTxCountMin(2);
    param.setTxCountMax(3);
    Assertions.assertTrue(param.isWithinTransactionCountBoundary(2));
    Assertions.assertFalse(
        param.isWithinTransactionCountBoundary(1),
        "Should fall outside of boundary range of 2 to 4");
    Assertions.assertFalse(
        param.isWithinTransactionCountBoundary(4),
        "Should fall outside of boundary range of 2 to 4");

    param = new QueryParams();
    param.setTxCountMax(2);
    Assertions.assertTrue(param.isWithinTransactionCountBoundary(2));
    Assertions.assertTrue(
        param.isWithinTransactionCountBoundary(1), "1 is below 2 so is within maximum boundary");
    Assertions.assertFalse(
        param.isWithinTransactionCountBoundary(4),
        "4 is above 2 so should not be within maximum boundary");

    param = new QueryParams();
    param.setTxCountMin(2);
    Assertions.assertTrue(param.isWithinTransactionCountBoundary(2));
    Assertions.assertFalse(
        param.isWithinTransactionCountBoundary(1),
        " 1 is below 2 so should fall outside of minimum boundary");
    Assertions.assertTrue(
        param.isWithinTransactionCountBoundary(4),
        " 4 is above 2 so should fall within minimum boundary");
  }
}
