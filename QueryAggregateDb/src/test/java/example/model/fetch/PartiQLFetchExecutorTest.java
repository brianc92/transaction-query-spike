package example.model.fetch;

import example.TestConfiguration;
import example.config.DynamoDbClientProvider;
import example.model.query.QueryParams;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

@MicronautTest
class PartiQLFetchExecutorTest {

  @Inject PartiQLFetchExecutor fetchExecutor;

  @Inject DynamoDbClientProvider clientProvider;

  @Test
  void testFetch() {
    var params = new QueryParams(2, "2021#04", "AMAZON", 5, 5);
    var results = fetchExecutor.fetch(clientProvider, params);
    Assertions.assertEquals(
        List.of(TestConfiguration.aggs[2]),
        results,
        "Count min and max boundaries should be inclusive");

    params = new QueryParams(2, "2021#04", "AMAZON", null, 5);
    results = fetchExecutor.fetch(clientProvider, params);
    Assertions.assertEquals(
        List.of(TestConfiguration.aggs[2], TestConfiguration.aggs[0]),
        results,
        "Results should return above transactionCount 5");

    params = new QueryParams(2, "2021#04", "AMAZON", 5, null);
    results = fetchExecutor.fetch(clientProvider, params);
    Assertions.assertEquals(
        List.of(TestConfiguration.aggs[2], TestConfiguration.aggs[1]),
        results,
        "Results should return below transactionCount 5");

    params = new QueryParams(2, "2021#06", "AMAZON", null, null);
    results = fetchExecutor.fetch(clientProvider, params);
    Assertions.assertEquals(
        List.of(TestConfiguration.aggs[0]), results, "Results should return most 'recent' month");
  }
}
