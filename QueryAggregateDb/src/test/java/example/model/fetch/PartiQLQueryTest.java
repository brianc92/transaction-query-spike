package example.model.fetch;

import example.config.Config;
import example.model.query.QueryParams;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

class PartiQLQueryTest {

  GetStatementTestExpectation[] testExpectations;

  int partiton = 1;

  @BeforeEach
  void before() {
    testExpectations = generateStatementTestExpectations();
  }

  @Test
  void testGetStatement() {
    for (var index = 0; index < testExpectations.length; index++) {
      var statement =
          new PartiQLQuery(testExpectations[index].params, testExpectations[index].partition)
              .getStatement();

      Assertions.assertEquals(
          testExpectations[index].expectedStatement,
          statement,
          "Generated statement to be as expected on index " + index);
    }
  }

  @Test
  void testGetAttributesAggIdRange() {

    var query = new PartiQLQuery(testExpectations[5].params, testExpectations[5].partition);
    var attributes = query.getAttributes();

    Assertions.assertEquals(
        testExpectations[5].params.getUserId() + "#1",
        attributes.get(0).s(),
        "User Id and Partition should match");
    Assertions.assertEquals(
        "agg#merchant#2021#01",
        attributes.get(1).s(),
        "Generated 'aggId >= ?' attribute should align with provided merchant and year-month");

    Assertions.assertEquals(
        "agg#merchant#2021#" + getRecentMonth(),
        attributes.get(2).s(),
        "Generated 'aggId <= ?' sort key should align with provided merchant and most recently completed year-month");

    Assertions.assertEquals(
        testExpectations[5].params.getTxCountMax().toString(),
        attributes.get(3).n(),
        "TxCountMax should match");
    Assertions.assertEquals(
        testExpectations[5].params.getTxCountMin().toString(),
        attributes.get(4).n(),
        "TxCountMin should match");
  }

  @Test
  void testGetPartitionedRequestsForQuery() {

    var params = new QueryParams(1, "2021#01", "merchant", 99, 1);
    var partitions = List.of(1, 2, 3);

    var partitionedRequests = PartiQLQuery.getPartitionedRequestsForQuery(params, partitions);

    for (var index = 0; index < partitionedRequests.size(); index++) {

      var partition = partitions.get(index);

      Assertions.assertEquals(
          "1#" + partition,
          partitionedRequests.get(index).parameters().get(0).s(),
          "Attribute for userId should match the partition");
    }
  }

  private String getRecentMonth() {
    return String.format("%02d", LocalDate.now().minusMonths(1).getMonthValue());
  }

  private GetStatementTestExpectation[] generateStatementTestExpectations() {
    return new GetStatementTestExpectation[] {
      new GetStatementTestExpectation(
          "select * from \"" + Config.TABLE_NAME + "\" where \"userId\"=? ",
          new QueryParams(1, null, null, null, null),
          1),
      new GetStatementTestExpectation(
          "select * from \""
              + Config.TABLE_NAME
              + "\" where \"userId\"=? AND \"aggId\" >= ? AND \"aggId\" <= ? ",
          new QueryParams(1, "date", "merchant", null, null),
          1),
      new GetStatementTestExpectation(
          "select * from \""
              + Config.TABLE_NAME
              + "\" where \"userId\"=? AND begins_with(\"aggId\", '"
              + Config.AGGREGATE_PREFIX
              + "?')",
          new QueryParams(1, null, "merchant", null, null),
          1),
      new GetStatementTestExpectation(
          "select * from \""
              + Config.TABLE_NAME
              + "\" where \"userId\"=? AND \"transactionCount\" <= ? ",
          new QueryParams(1, null, null, 99, null),
          1),
      new GetStatementTestExpectation(
          "select * from \""
              + Config.TABLE_NAME
              + "\" where \"userId\"=? AND \"transactionCount\" >= ? ",
          new QueryParams(1, null, null, null, 1),
          1),
      new GetStatementTestExpectation(
          "select * from \""
              + Config.TABLE_NAME
              + "\" where \"userId\"=? AND \"aggId\" >= ? AND \"aggId\" <= ? AND \"transactionCount\" <= ? AND \"transactionCount\" >= ? ",
          new QueryParams(1, "2021#01", "merchant", 99, 1),
          1)
    };
  }

  public static class GetStatementTestExpectation {
    final String expectedStatement;
    final QueryParams params;
    final Integer partition;

    public GetStatementTestExpectation(
        String expectedStatement, QueryParams params, Integer partition) {
      this.expectedStatement = expectedStatement;
      this.params = params;
      this.partition = partition;
    }
  }
}
