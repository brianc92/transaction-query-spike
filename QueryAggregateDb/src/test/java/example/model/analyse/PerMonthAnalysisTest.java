package example.model.analyse;

import example.model.AggregateData;
import example.model.query.QueryParams;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class PerMonthAnalysisTest {

  PerMonthAnalysis perMonthAnalysis = new PerMonthAnalysis();

  List<AggregateData> transactionData =
      List.of(
          new AggregateData("user", "sortkey", "merch", 1),
          new AggregateData("user", "sortkey", "merch", 2),
          new AggregateData("user", "sortkey", "merch", 2));

  @Test
  void testAnalyse() {

    var params = new QueryParams();
    params.setMonthsBacktrack(3);
    Assertions.assertTrue(
        perMonthAnalysis.analyse(params, transactionData),
        "Should return true as number of months collected equals number of months backtacked");

    params = new QueryParams();
    params.setMonthsBacktrack(4);
    Assertions.assertFalse(
        perMonthAnalysis.analyse(params, transactionData),
        "Should return false as number of months back tracked is higher than months collected");
  }
}
