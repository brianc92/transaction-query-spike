package example.model.analyse;

import example.model.AggregateData;
import example.model.query.QueryParams;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class SpanAllMonthsAnalysisTest {

  SpanAllMonthsAnalysis spanAllMonthsAnalysis = new SpanAllMonthsAnalysis();

  List<AggregateData> transactionData =
      List.of(
          new AggregateData("user", "sortkey", "merch", 1),
          new AggregateData("user", "sortkey", "merch", 2),
          new AggregateData("user", "sortkey", "merch", 2));

  @Test
  void testAnalyse() {
    var params = new QueryParams();
    params.setTxCountMin(5);
    Assertions.assertTrue(
        spanAllMonthsAnalysis.analyse(params, transactionData),
        "Should return true as Count minimum value is equal the actual count");

    params = new QueryParams();
    params.setTxCountMin(6);
    Assertions.assertFalse(
        spanAllMonthsAnalysis.analyse(params, transactionData),
        "Should return false as count minimum is above the actual count");

    params = new QueryParams();
    params.setTxCountMax(5);
    Assertions.assertTrue(
        spanAllMonthsAnalysis.analyse(params, transactionData),
        "Should return true as Count maximum value is equal the actual count");

    params = new QueryParams();
    params.setTxCountMax(4);
    Assertions.assertFalse(
        spanAllMonthsAnalysis.analyse(params, transactionData),
        "Should return false as Count maximum value is less than the actual count");

    params = new QueryParams();
    params.setTxCountMin(5);
    params.setTxCountMax(5);
    Assertions.assertTrue(
        spanAllMonthsAnalysis.analyse(params, transactionData),
        "Should return true as Count minimum value and count maximum are both equal the actual count");

    params = new QueryParams();
    params.setTxCountMin(6);
    params.setTxCountMax(6);
    Assertions.assertFalse(
        spanAllMonthsAnalysis.analyse(params, transactionData),
        "Should return false as count minimum is above the actual count");

    params = new QueryParams();
    params.setTxCountMin(4);
    params.setTxCountMax(4);
    Assertions.assertFalse(
        spanAllMonthsAnalysis.analyse(params, transactionData),
        "Should return false as count maximum is below the actual count");
  }
}
