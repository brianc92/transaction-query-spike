package example.service;

import example.config.DynamoDbClientProvider;
import example.model.RuleV2;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

@MicronautTest
class TransactionQueryServiceTest {

  @Test
  void applyRule() {}

  @Inject TransactionQueryService queryService;

  @Inject DynamoDbClientProvider clientProvider;

  @Test
  void testFetch() {
    var rule = new RuleV2();
    rule.setMerchant("AMAZON");
    rule.setMustAppearInLastXMonths(2);
    Assertions.assertTrue(queryService.applyRule(rule, 2), "Amazon should appear in last 2 months");

    rule.setMustAppearInLastXMonths(4);
    Assertions.assertTrue(
        queryService.applyRule(rule, 2),
        "Amazon should appear in last 4 months, while there are only 3 months of data");
  }
}
