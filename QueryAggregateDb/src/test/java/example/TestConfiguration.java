package example;

import com.amazonaws.services.dynamodbv2.local.embedded.DynamoDBEmbedded;
import example.config.Config;
import example.config.DynamoDbClientProvider;
import example.model.AggregateData;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Replaces;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.*;

import javax.inject.Singleton;
import java.util.Arrays;
import java.util.Map;

@Factory
public class TestConfiguration {

  public static final AggregateData[] aggs =
      new AggregateData[] {
        new AggregateData("2#1", Config.AGGREGATE_PREFIX + "AMAZON#2021#06", "AMAZON", 16),
        new AggregateData("2#1", Config.AGGREGATE_PREFIX + "AMAZON#2021#05", "AMAZON", 1),
        new AggregateData("2#1", Config.AGGREGATE_PREFIX + "AMAZON#2021#04", "AMAZON", 5)
      };

  @Singleton
  @Replaces(DynamoDbClient.class)
  public DynamoDbClient dynamoDbClient() {
    return DynamoDBEmbedded.create().dynamoDbClient();
  }

  @Singleton
  @Replaces(DynamoDbClientProvider.class)
  public DynamoDbClientProvider dynamoClientProvider() {
    DynamoDbClientProvider provider = new DynamoDbClientProvider(dynamoDbClient());

    provider
        .getDynamoClient()
        .createTable(
            CreateTableRequest.builder()
                .tableName(Config.TABLE_NAME)
                .keySchema(
                    KeySchemaElement.builder()
                        .attributeName("userId")
                        .keyType(KeyType.HASH)
                        .build(),
                    KeySchemaElement.builder()
                        .attributeName("aggId")
                        .keyType(KeyType.RANGE)
                        .build())
                .attributeDefinitions(
                    AttributeDefinition.builder()
                        .attributeName("aggId")
                        .attributeType(ScalarAttributeType.S)
                        .build(),
                    AttributeDefinition.builder()
                        .attributeName("userId")
                        .attributeType(ScalarAttributeType.S)
                        .build())
                .provisionedThroughput(
                    ProvisionedThroughput.builder()
                        .readCapacityUnits(5L)
                        .writeCapacityUnits(5L)
                        .build())
                .build());

    //      provider.getDynamoClient().putItem(PutItemRequest.builder()
    //              .item(toMap(aggs.get(0)))
    //              .build())

    //      provider.getAggregationTable().createTable();
    Arrays.stream(aggs)
        .forEach(
            agg ->
                provider
                    .getDynamoClient()
                    .putItem(
                        PutItemRequest.builder()
                            .tableName(Config.TABLE_NAME)
                            .item(toMap(agg))
                            .build()));

    return provider;
  }

  private Map<String, AttributeValue> toMap(AggregateData aggregateData) {
    return Map.of(
        "userId",
        AttributeValue.builder().s(aggregateData.getUserId()).build(),
        "aggId",
        AttributeValue.builder().s(aggregateData.getAggId()).build(),
        "merchant",
        AttributeValue.builder().s(aggregateData.getMerchant()).build(),
        "transactionCount",
        AttributeValue.builder().n(aggregateData.getTransactionCount().toString()).build());
  }
}
