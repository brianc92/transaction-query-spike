package example.config;

import example.model.AggregateData;
import lombok.Getter;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
@Getter
public class DynamoDbClientProvider {

  public static final String TX_TABLE_NAME = "TransactionDataTable-spike";

  private DynamoDbClient dynamoClient;

  private DynamoDbEnhancedClient enhancedClient;

  @Inject
  public DynamoDbClientProvider(DynamoDbClient dynamoClient) {
    this.dynamoClient = dynamoClient;
    enhancedClient = DynamoDbEnhancedClient.builder().dynamoDbClient(this.dynamoClient).build();
  }

  public DynamoDbTable<AggregateData> getAggregationTable() {
    return enhancedClient.table(TX_TABLE_NAME, TableSchema.fromBean(AggregateData.class));
  }
}
