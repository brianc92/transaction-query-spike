package example.config;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Config {
  public static final String DELIMINATOR = "#";

  public static String AGGREGATE_PREFIX = "agg" + DELIMINATOR;

  public static final int PARTITION_MIN = 1;

  public static final int PARTITION_MAX = 3;

  public static final String TABLE_NAME = "TransactionDataTable-spike";

  public static final List<Integer> PARTITIONS =
      IntStream.rangeClosed(Config.PARTITION_MIN, Config.PARTITION_MAX)
          .boxed()
          .collect(Collectors.toUnmodifiableList());

  private Config() {}
}
