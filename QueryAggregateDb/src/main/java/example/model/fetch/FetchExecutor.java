package example.model.fetch;

import example.config.DynamoDbClientProvider;
import example.model.AggregateData;
import example.model.query.QueryParams;

import java.util.List;

public interface FetchExecutor {
  List<AggregateData> fetch(DynamoDbClientProvider clientProvider, QueryParams params);
}
