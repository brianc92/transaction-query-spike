package example.model.fetch;

import example.config.Config;
import example.model.query.QueryParams;
import lombok.Getter;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.ExecuteStatementRequest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PartiQLQuery {

  private final String baseQuery =
      "select * from \"" + Config.TABLE_NAME + "\" where \"userId\"=? ";

  private final String aggIdStartsWithQuery =
      "AND begins_with(\"aggId\", '" + Config.AGGREGATE_PREFIX + "?')";

  private final String betweenDateForMerchant = "AND \"aggId\" >= ? AND \"aggId\" <= ? ";

  private final String transactionCountMaxQuery = "AND \"transactionCount\" <= ? ";
  private final String transactionCountMinAmount = "AND \"transactionCount\" >= ? ";

  @Getter private final QueryParams params;
  private final StringBuilder queryString = new StringBuilder(baseQuery);
  private final List<AttributeValue> attributes = new ArrayList<>();
  private final int partition;

  public PartiQLQuery(QueryParams params, int partition) {
    this.params = params;
    this.partition = partition;
    build();
  }

  /***
   * Generates a list of PartiQL queries differentiated by partition for the provided query parameters
   *
   * @param params - the parameters for the partiQl statement
   * @param partitions - the partitions to target
   * @return List<ExecuteStatementRequest> - list of ExecuteStatementRequests for the provided query differentiated by partition
   */
  public static List<ExecuteStatementRequest> getPartitionedRequestsForQuery(
      QueryParams params, List<Integer> partitions) {
    var requests = new ArrayList<ExecuteStatementRequest>();
    for (var partition : partitions) {
      requests.add(new PartiQLQuery(params, partition).getExecuteStatementRequest());
    }
    return requests;
  }

  private void build() {
    Objects.requireNonNull(params.getUserId());
    attributes.add(
        AttributeValue.builder().s(params.getUserId() + Config.DELIMINATOR + partition).build());

    // if date and merchant are provided then backtrack
    // to from to that date from the last complete month
    if (params.getDate() != null && params.getMerchant() != null) {
      queryString.append(betweenDateForMerchant);
      attributes.add(AttributeValue.builder().s(buildBackdateString(params)).build());
      attributes.add(AttributeValue.builder().s(buildCurrentDateString(params)).build());

      // else if just merchant is provided the grab all for that merchant
    } else if (params.getMerchant() != null) {
      queryString.append(aggIdStartsWithQuery);
      attributes.add(AttributeValue.builder().s(params.getDate()).build());
    }

    // filter aggregates by tx count
    if (params.getTxCountMax() != null) {
      queryString.append(transactionCountMaxQuery);
      attributes.add(AttributeValue.builder().n(params.getTxCountMax().toString()).build());
    }
    if (params.getTxCountMin() != null) {
      queryString.append(transactionCountMinAmount);
      attributes.add(AttributeValue.builder().n(params.getTxCountMin().toString()).build());
    }
  }

  // As agg data is stored in months, this gets the most recently completed month
  private String buildCurrentDateString(QueryParams params) {
    var date = LocalDate.now().minusMonths(1);

    return String.format(
        "%s%s%s%d%s%02d",
        Config.AGGREGATE_PREFIX,
        params.getMerchant(),
        Config.DELIMINATOR,
        date.getYear(),
        Config.DELIMINATOR,
        date.getMonthValue());
  }

  private String buildBackdateString(QueryParams params) {
    return String.format(
        "%s%s%s%s",
        Config.AGGREGATE_PREFIX, params.getMerchant(), Config.DELIMINATOR, params.getDate());
  }

  public ExecuteStatementRequest getExecuteStatementRequest() {
    return ExecuteStatementRequest.builder()
        .statement(getStatement())
        .parameters(getAttributes())
        .build();
  }

  public String getStatement() {
    return queryString.toString();
  }

  public List<AttributeValue> getAttributes() {
    return attributes;
  }
}
