package example.model.fetch;

import example.config.Config;
import example.config.DynamoDbClientProvider;
import example.model.AggregateData;
import example.model.query.QueryParams;

import javax.inject.Singleton;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class PartiQLFetchExecutor implements FetchExecutor {

  /**
   * Fetches all the AggregateData that apply to the query params
   *
   * @param clientProvider - dynamoDb client
   * @param params - query parameters
   * @return List<AggregateData>
   */
  public List<AggregateData> fetch(DynamoDbClientProvider clientProvider, QueryParams params) {
    // gets a query for each partition in parallel, serialised them into an AggregateData dto and
    // bundles them togther into a list
    return PartiQLQuery.getPartitionedRequestsForQuery(params, Config.PARTITIONS).parallelStream()
        .flatMap(query -> clientProvider.getDynamoClient().executeStatement(query).items().stream())
        .map(a -> AggregateData.of(a))
        .collect(Collectors.toList());
  }
}
