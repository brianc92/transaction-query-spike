package example.model.analyse;

import example.model.AggregateData;
import example.model.query.QueryParams;

import java.util.List;

public interface AnalyseAggregatesFunction {

  boolean analyse(QueryParams params, List<AggregateData> transactions);
}
