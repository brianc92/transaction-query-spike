package example.model.analyse;

import example.model.AggregateData;
import example.model.query.QueryParams;

import java.util.List;

/**
 * Add up the total amount of transactions in each Aggregate and compare to requested min / max
 * bounds
 */
public class SpanAllMonthsAnalysis implements AnalyseAggregatesFunction {
  @Override
  public boolean analyse(QueryParams params, List<AggregateData> transactions) {

    var total = 0;

    for (var agg : transactions) {
      total += agg.getTransactionCount();
    }

    return params.isWithinTransactionCountBoundary(total);
  }
}
