package example.model.analyse;

import example.model.AggregateData;
import example.model.query.QueryParams;

import java.util.List;

/**
 * Checks that each month falls within the transaction boundries
 *
 * <p>Since the PartiQLQuery has already filtered out months that do not meet the transaction count
 * boundries, this function only checks if there are any months missing. If there arr months
 * missing, then return false
 */
public class PerMonthAnalysis implements AnalyseAggregatesFunction {

  @Override
  public boolean analyse(QueryParams params, List<AggregateData> transactions) {
    return transactions.size() == params.getMonthsBacktrack();
  }
}
