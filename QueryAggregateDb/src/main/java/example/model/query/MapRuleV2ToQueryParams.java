package example.model.query;

import example.config.Config;
import example.model.RuleV2;

import java.time.LocalDate;

public class MapRuleV2ToQueryParams {

  // Mapping to mustAppearInLastXMonths for MVP
  public QueryParams mapRule(RuleV2 rule, Integer userId) {
    var params = new QueryParams();
    params.setUserId(userId);
    params.setDate(generateDate(rule));
    params.setMerchant(rule.getMerchant());
    params.setMonthsBacktrack(rule.getMustOccurXTimesPerMonth());

    // since were looking at MustAppearInLastXMonths, set to zero
    // but changing params.setTxCountMin will facilitate the idea of 'must appear y times in x
    // months'
    // changing params.setTxCountMax will facilitate must 'appear less than y times in x momths'
    // setting both should be like 'must appear between y and z times in x months'
    params.setTxCountMin(0);

    // setting to analyse all the months collected together
    // can also set to PER_MONTH to apply the minimum count and maximum count on a per month bases
    params.setAnalysisMethod(AnalysisMethod.PER_SPAN_OF_MONTHS);
    return params;
  }

  private String generateDate(RuleV2 rule) {
    var date = LocalDate.now().minusMonths(rule.getMustAppearInLastXMonths());

    return String.format("%d%s%02d", date.getYear(), Config.DELIMINATOR, date.getMonthValue());
  }
}
