package example.model.query;

import example.model.RuleV2;
import io.micronaut.core.annotation.Introspected;
import lombok.*;

@Introspected
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class QueryParams {

  private Integer userId;
  private String date;
  private String merchant;
  private Integer txValueMax;
  private Integer txValueMin;
  private Integer monthsBacktrack;
  private Integer txCountMax;
  private Integer txCountMin;
  private AnalysisMethod analysisMethod;

  public QueryParams(
      Integer userId, String date, String merchant, Integer txCountMax, Integer txCountMin) {
    this.userId = userId;
    this.date = date;
    this.merchant = merchant;
    this.txCountMax = txCountMax;
    this.txCountMin = txCountMin;
  }

  public static QueryParams of(RuleV2 rule, Integer userId) {
    return new MapRuleV2ToQueryParams().mapRule(rule, userId);
  }

  public boolean isWithinTransactionCountBoundary(int count) {
    if (getTxCountMin() != null && getTxCountMax() != null)
      return getTxCountMin() <= count && getTxCountMax() >= count;
    if (getTxCountMin() != null) return getTxCountMin() <= count;
    if (getTxCountMax() != null) return getTxCountMax() >= count;

    throw new IllegalArgumentException("Transaction count boundries not set");
  }
}
