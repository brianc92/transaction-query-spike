package example.model.query;

import example.model.AggregateData;
import example.model.analyse.AnalyseAggregatesFunction;
import example.model.analyse.PerMonthAnalysis;
import example.model.analyse.SpanAllMonthsAnalysis;

import java.util.List;

/** Maps the analysis method to an implementation */
public enum AnalysisMethod implements AnalyseAggregatesFunction {
  PER_MONTH {
    @Override
    public boolean analyse(QueryParams params, List<AggregateData> transactions) {
      return new PerMonthAnalysis().analyse(params, transactions);
    }
  },
  PER_SPAN_OF_MONTHS {
    @Override
    public boolean analyse(QueryParams params, List<AggregateData> transactions) {
      return new SpanAllMonthsAnalysis().analyse(params, transactions);
    }
  };
}
