package example.model;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbSortKey;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import java.util.Map;
import java.util.Objects;

/***
 * DTO representing a row of data from the AggregateData table
 *
 */
@DynamoDbBean
@Introspected
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AggregateData {
  private String userId;
  private String aggId;
  private String merchant;
  private Integer transactionCount;

  @DynamoDbPartitionKey
  public String getUserId() {
    return userId;
  }

  @DynamoDbSortKey
  public String getAggId() {
    return aggId;
  }

  public static AggregateData of(Map<String, AttributeValue> attributeMap) {
    var data = new AggregateData();
    data.setUserId(attributeMap.get("userId").s());
    data.setAggId(attributeMap.get("aggId").s());
    data.setMerchant(attributeMap.get("merchant").s());
    data.setTransactionCount(Integer.parseInt(attributeMap.get("transactionCount").n()));
    return data;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AggregateData that = (AggregateData) o;
    return getUserId().equals(that.getUserId()) && getAggId().equals(that.getAggId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getUserId(), getAggId());
  }
}
