package example.model;

import example.config.Config;
import example.model.fetch.PartiQLQuery;
import example.model.query.QueryParams;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PartiQLQueryTest {

  QueryParams params;

  GetStatementTestExpectation[] testExpectations;

  int partiton = 1;

  @BeforeEach
  void before() {
    testExpectations = generateStatementTestExpectations();
  }

  @Test
  void testGetStatement() {
    for (var index = 0; index < testExpectations.length; index++) {
      var statement =
          new PartiQLQuery(testExpectations[index].params, testExpectations[index].partition)
              .getStatement();

      Assertions.assertEquals(
          testExpectations[index].expectedStatement,
          statement,
          "Generated statement to be as expected on index " + index);
    }
  }

  @Test
  void testGetAttributes() {

    var query = new PartiQLQuery(testExpectations[5].params, testExpectations[5].partition);
    var attributes = query.getAttributes();

    Assertions.assertEquals(
        testExpectations[5].params.getUserId() + "#1",
        attributes.get(0).s(),
        "User Id and Partition should match");
    Assertions.assertEquals(
        testExpectations[5].params.getDate(), attributes.get(1).s(), "Date should match");
    Assertions.assertEquals(
        testExpectations[5].params.getMerchant(), attributes.get(2).s(), "Merchant should match");
    Assertions.assertEquals(
        testExpectations[5].params.getTxValueMax().toString(),
        attributes.get(3).n(),
        "TxValueMax should match");
    Assertions.assertEquals(
        testExpectations[5].params.getTxValueMin().toString(),
        attributes.get(4).n(),
        "TxValueMin should match");
  }

  private GetStatementTestExpectation[] generateStatementTestExpectations() {
    return new GetStatementTestExpectation[] {
      new GetStatementTestExpectation(
          "select * from \"" + Config.TABLE_NAME + "\" where \"userId\"=? ",
          new QueryParams(1, null, null, null, null),
          1),
      new GetStatementTestExpectation(
          "select * from \"" + Config.TABLE_NAME + "\" where \"userId\"=? AND  \"date\" > ? ",
          new QueryParams(1, "date", null, null, null),
          1),
      new GetStatementTestExpectation(
          "select * from \"" + Config.TABLE_NAME + "\" where \"userId\"=? AND  \"merchant\" = ? ",
          new QueryParams(1, null, "merchant", null, null),
          1),
      new GetStatementTestExpectation(
          "select * from \"" + Config.TABLE_NAME + "\" where \"userId\"=? AND  \"amount\" < ? ",
          new QueryParams(1, null, null, 99, null),
          1),
      new GetStatementTestExpectation(
          "select * from \"" + Config.TABLE_NAME + "\" where \"userId\"=? AND  \"amount\" > ? ",
          new QueryParams(1, null, null, null, 1),
          1),
      new GetStatementTestExpectation(
          "select * from \""
              + Config.TABLE_NAME
              + "\" where \"userId\"=? AND  \"date\" > ? AND  \"merchant\" = ? AND  \"amount\" < ? AND  \"amount\" > ? ",
          new QueryParams(1, "date", "merchant", 99, 1),
          1)
    };
  }

  public static class GetStatementTestExpectation {
    final String expectedStatement;
    final QueryParams params;
    final Integer partition;

    public GetStatementTestExpectation(
        String expectedStatement, QueryParams params, Integer partition) {
      this.expectedStatement = expectedStatement;
      this.params = params;
      this.partition = partition;
    }
  }
}
