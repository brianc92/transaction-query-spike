package example.model.analyse;

import example.model.query.QueryParams;

class PerTimeUnitAnalysisTest {}

class TestData {
  QueryParams params;
  boolean expectedResult;

  public TestData(QueryParams params, boolean expectedResult) {
    this.params = params;
    this.expectedResult = expectedResult;
  }
}
