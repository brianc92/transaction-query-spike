package example.model;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Introspected
public class RuleV2 {

  private String id;
  private String ruleName;
  private String merchant;
  private Double minSpend;
  private Double maxSpend;
  private Integer mustOccurXTimesPerMonth;
  private Integer mustOccurOncePerMonthForXMonths;
  private Integer mustSpendMoreThanXPerMonths;
  private Integer mustAppearInLastXMonths;
}
