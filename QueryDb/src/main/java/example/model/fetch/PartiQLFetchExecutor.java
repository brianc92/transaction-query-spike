package example.model.fetch;

import example.config.Config;
import example.config.DynamoDbClientProvider;
import example.model.TransactionData;
import example.model.query.QueryParams;

import javax.inject.Singleton;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class PartiQLFetchExecutor implements FetchExecutor {

  public List<TransactionData> fetch(DynamoDbClientProvider clientProvider, QueryParams params) {
    return PartiQLQuery.getPartitionedRequestsForQuery(params, Config.PARTITIONS).parallelStream()
        .flatMap(query -> clientProvider.getDynamoClient().executeStatement(query).items().stream())
        .map(a -> TransactionData.of(a))
        .collect(Collectors.toList());
  }
}
