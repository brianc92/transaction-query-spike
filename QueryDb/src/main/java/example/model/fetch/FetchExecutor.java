package example.model.fetch;

import example.config.DynamoDbClientProvider;
import example.model.TransactionData;
import example.model.query.QueryParams;

import java.util.List;

public interface FetchExecutor {
  List<TransactionData> fetch(DynamoDbClientProvider clientProvider, QueryParams params);
}
