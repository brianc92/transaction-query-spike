package example.model.fetch;

import example.config.Config;
import example.model.query.QueryParams;
import lombok.Getter;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.ExecuteStatementRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PartiQLQuery {

  private final String baseQuery =
      "select * from \"" + Config.TABLE_NAME + "\" where \"userId\"=? ";

  private final String dateQuery = "AND  \"date\" > ? ";
  private final String merchantQuery = "AND  \"merchant\" = ? ";
  private final String txValueMaxQuery = "AND  \"amount\" < ? ";
  private final String txValueMinAmount = "AND  \"amount\" > ? ";

  @Getter private final QueryParams params;
  private final StringBuilder queryString = new StringBuilder(baseQuery);
  private final List<AttributeValue> attributes = new ArrayList<>();
  private final int partition;

  public PartiQLQuery(QueryParams params, int partition) {
    this.params = params;
    this.partition = partition;
    build();
  }

  /***
   * Generates a list of PartiQL queries differentiated by partition for the provided query parameters
   *
   * @param params - the parameters for the partiQl statement
   * @param partitions - the partitions to target
   * @return List<ExecuteStatementRequest> - list of ExecuteStatementRequests for the provided query differentiated by partition
   */
  static List<ExecuteStatementRequest> getPartitionedRequestsForQuery(
      QueryParams params, List<Integer> partitions) {
    var requests = new ArrayList<ExecuteStatementRequest>();
    for (var partition : partitions) {
      requests.add(new PartiQLQuery(params, partition).getExecuteStatementRequest());
    }
    return requests;
  }

  private void build() {
    Objects.requireNonNull(params.getUserId());
    attributes.add(
        AttributeValue.builder().s(params.getUserId() + Config.DELIMINATOR + partition).build());

    if (params.getDate() != null) {
      queryString.append(dateQuery);
      attributes.add(AttributeValue.builder().s(params.getDate()).build());
    }
    if (params.getMerchant() != null) {
      queryString.append(merchantQuery);
      attributes.add(AttributeValue.builder().s(params.getMerchant()).build());
    }
    if (params.getTxValueMax() != null) {
      queryString.append(txValueMaxQuery);
      attributes.add(AttributeValue.builder().n(params.getTxValueMax().toString()).build());
    }
    if (params.getTxValueMin() != null) {
      queryString.append(txValueMinAmount);
      attributes.add(AttributeValue.builder().n(params.getTxValueMin().toString()).build());
    }
  }

  public ExecuteStatementRequest getExecuteStatementRequest() {
    return ExecuteStatementRequest.builder()
        .statement(getStatement())
        .parameters(getAttributes())
        .build();
  }

  public String getStatement() {
    return queryString.toString();
  }

  public List<AttributeValue> getAttributes() {
    return attributes;
  }
}
