package example.model;

import lombok.*;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbSortKey;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

import java.util.Map;

@DynamoDbBean
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TransactionData {

  private String userId;
  private String date;
  private String merchant;
  private String transaction;
  private String transactionHash;
  private Integer day;
  private Integer month;
  private Integer year;
  private Double amount;

  @DynamoDbPartitionKey
  public String getUserId() {
    return userId;
  }

  @DynamoDbSortKey
  public String getDate() {
    return date;
  }

  public static TransactionData of(Map<String, AttributeValue> attributeMap) {
    var data = new TransactionData();
    data.setUserId(attributeMap.get("userId").s());
    data.setDate(attributeMap.get("date").s());
    data.setMonth(Integer.parseInt(attributeMap.get("month").n()));
    data.setYear(Integer.parseInt(attributeMap.get("year").n()));
    data.setAmount(Double.parseDouble(attributeMap.get("amount").n()));
    return data;
  }
}
