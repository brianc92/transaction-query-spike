package example.model.query;

import example.model.TransactionData;
import example.model.analyse.AnalyseTransactionsFunction;
import example.model.analyse.PerTimeSpanAnalysis;
import example.model.analyse.PerTimeUnitAnalysis;

import java.util.List;

public enum TxCountMethod implements AnalyseTransactionsFunction {
  PER_UNIT {
    @Override
    public boolean analyse(QueryParams params, List<TransactionData> transactions) {
      return new PerTimeUnitAnalysis().analyse(params, transactions);
    }
  },
  PER_SPAN {
    @Override
    public boolean analyse(QueryParams params, List<TransactionData> transactions) {
      return new PerTimeSpanAnalysis().analyse(params, transactions);
    }
  };
}
