package example.model.query;

import example.config.Config;
import example.model.RuleV2;

import java.time.LocalDate;

public class MapRuleV2ToQueryParams {

  // Mapping to mustAppearInLastXMonths for MVP
  public QueryParams mapRule(RuleV2 rule, Integer userId) {
    var params = new QueryParams();
    params.setUserId(userId);
    params.setDate(generateDate(rule));
    params.setMerchant(rule.getMerchant());
    params.setTxCountMethod(TxCountMethod.PER_SPAN);
    params.setTxCountMin(0);
    return params;
  }

  private String generateDate(RuleV2 rule) {
    var date = LocalDate.now().minusMonths(rule.getMustAppearInLastXMonths());

    return String.format(
        "%d%s%02d%s%02d",
        date.getYear(),
        Config.DELIMINATOR,
        date.getMonthValue(),
        Config.DELIMINATOR,
        date.getDayOfMonth());
  }
}
