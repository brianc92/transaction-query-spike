package example.model.query;

import example.model.RuleV2;
import io.micronaut.core.annotation.Introspected;
import lombok.*;

@Introspected
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class QueryParams {

  private Integer userId;
  private String date;
  private String merchant;
  private Integer txValueMax;
  private Integer txValueMin;

  private Integer txCountMax;
  private Integer txCountMin;
  private TxCountMethod txCountMethod;

  public QueryParams(
      Integer userId, String date, String merchant, Integer txValueMax, Integer txValueMin) {
    this.userId = userId;
    this.date = date;
    this.merchant = merchant;
    this.txValueMax = txValueMax;
    this.txValueMin = txValueMin;
  }

  public static QueryParams of(RuleV2 rule, Integer userId) {
    return new MapRuleV2ToQueryParams().mapRule(rule, userId);
  }
}
