package example.model.analyse;

import example.model.TransactionData;
import example.model.query.QueryParams;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class PerTimeUnitAnalysis implements AnalyseTransactionsFunction {

  private final PerTimeSpanAnalysis timeSpanAnaysis = new PerTimeSpanAnalysis();

  @Override
  public boolean analyse(QueryParams params, List<TransactionData> transactions) {
    if (transactions.isEmpty()) return false;

    var monthMap = mapTransactionsToYearMonth(transactions);

    /*
    account for:
      - months are missing
      - month doesnt follow query params
     */

    // monthMap.keySet().size().equals(params.getSpanOfTimeUnits()) <- coming from Rule
    return monthMap.values().stream()
        .filter(list -> !timeSpanAnaysis.analyse(params, list))
        .collect(Collectors.toList())
        .isEmpty();
  }

  private HashMap<String, List<TransactionData>> mapTransactionsToYearMonth(
      List<TransactionData> transactions) {
    var monthMap = new HashMap<String, List<TransactionData>>();
    for (var transactionData : transactions) {
      monthMap.putIfAbsent(
          transactionData.getYear().toString() + transactionData.getMonth().toString(),
          newList(transactionData));

      monthMap
          .get(transactionData.getYear().toString() + transactionData.getMonth().toString())
          .add(transactionData);
    }
    return monthMap;
  }

  private ArrayList<TransactionData> newList(TransactionData data) {
    var list = new ArrayList<TransactionData>();
    list.add(data);
    return list;
  }
}
