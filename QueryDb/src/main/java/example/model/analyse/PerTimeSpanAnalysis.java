package example.model.analyse;

import example.model.TransactionData;
import example.model.query.QueryParams;

import java.util.List;

public class PerTimeSpanAnalysis implements AnalyseTransactionsFunction {
  @Override
  public boolean analyse(QueryParams params, List<TransactionData> transactions) {
    if (params.getTxCountMin() != null && params.getTxCountMax() != null) {
      return transactions.size() >= params.getTxCountMin()
          && transactions.size() <= params.getTxCountMax();
    } else if (params.getTxCountMin() != null) {
      return transactions.size() >= params.getTxCountMin();
    } else if (params.getTxCountMax() != null) {
      return transactions.size() <= params.getTxCountMax();
    }
    return false;
  }
}
