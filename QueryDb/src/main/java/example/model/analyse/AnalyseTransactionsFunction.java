package example.model.analyse;

import example.model.TransactionData;
import example.model.query.QueryParams;

import java.util.List;

public interface AnalyseTransactionsFunction {

  boolean analyse(QueryParams params, List<TransactionData> transactions);
}
