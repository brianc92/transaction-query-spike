package example.service;

import example.config.DynamoDbClientProvider;
import example.model.RuleV2;
import example.model.fetch.FetchExecutor;
import example.model.query.QueryParams;
import lombok.AllArgsConstructor;

import javax.inject.Singleton;

@Singleton
@AllArgsConstructor
public class TransactionQueryService {

  private DynamoDbClientProvider dynamoDbClientProvider;

  private FetchExecutor partiQLFetchExecutor;

  public boolean applyRule(RuleV2 rule, Integer userId) {
    var params = QueryParams.of(rule, userId);
    var transactions = partiQLFetchExecutor.fetch(dynamoDbClientProvider, params);
    return params.getTxCountMethod().analyse(params, transactions);
  }
}
