package example;

import example.model.RuleV2;
import example.service.TransactionQueryService;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.function.aws.MicronautRequestHandler;
import io.micronaut.runtime.Micronaut;

import javax.inject.Inject;

@Introspected
public class TransactionQueryHandler extends MicronautRequestHandler<RuleV2, Object> {

  @Inject TransactionQueryService service;

  public static void main(String[] args) {
    //    var params = new QueryParams();
    //
    //    params.setUserId(2);
    //    params.setDate("2021#05#01");
    //    params.setMerchant("MCDONALD'S");
    //    params.setTxCountMethod(TxCountMethod.PER_UNIT);
    //    params.setTxCountMin(4);

    var rule = new RuleV2("id", "test_mcdonalds", "MCDONALD'S", null, null, null, null, null, 3);

    Micronaut.run(TransactionQueryHandler.class, args)
        .getBean(TransactionQueryHandler.class)
        .execute(rule);
  }

  @Override
  public Object execute(RuleV2 rule) {
    return service.applyRule(rule, 2);
  }
}
