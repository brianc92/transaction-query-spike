package example;

import java.util.concurrent.ThreadLocalRandom;

public class Util {

    static String generatePartitionSuffix(){
        return Config.DELIMINATOR + ThreadLocalRandom.current().nextInt(Config.PARTITION_MIN, Config.PARTITION_MAX + 1);
    }
}
