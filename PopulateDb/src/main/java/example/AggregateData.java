package example;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbSortKey;

@DynamoDbBean
@Introspected
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AggregateData {
  private String userId;
  private String aggId;
  private String merchant;
  private Integer transactionCount;

  @DynamoDbPartitionKey
  public String getUserId() {
    return userId;
  }

  @DynamoDbSortKey
  public String getAggId() {
    return aggId;
  }
}
