package example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.function.aws.MicronautRequestHandler;
import io.micronaut.runtime.Micronaut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

import javax.inject.Inject;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static example.Config.AGGREGATE_PREFIX;

@Introspected
public class PopulateDynamoDbUtility extends MicronautRequestHandler<Object, Object> {

  Logger log = LoggerFactory.getLogger(PopulateDynamoDbUtility.class);

  @Inject private DynamoDbClient dynamoClient;

  public static void main(String[] args) {
    //    var params = new QueryParams();
    //
    //    params.setUserId(2);
    //    params.setDate("2021#05#01");
    //    params.setMerchant("MCDONALD'S");
    //    params.setTxCountMethod(TxCountMethod.PER_UNIT);
    //    params.setTxCountMin(4);

    Micronaut.run(PopulateDynamoDbUtility.class, args)
        .getBean(PopulateDynamoDbUtility.class)
        .populate("rule");
  }

  @Override
  public Object execute(Object input) {

    var enhancedClient = DynamoDbEnhancedClient.builder().dynamoDbClient(this.dynamoClient).build();

    var tableName = "user-transactions"; // System.getenv("BigNumberTableName");
    var originTable =
        enhancedClient.table(tableName, TableSchema.fromBean(OriginTransactionData.class));

    //    var destTableName = "TransactionDataTable-spike";
    //    var destinationTable =
    //        enhancedClient.table(destTableName,
    // TableSchema.fromBean(DestinationTransactionData.class));

    var result = originTable.scan();

    //    for (var page : result) {
    //
    //    }

    for (var data : result.items()) {
      // var destTx = DestinationTransactionData.of(data);

      log.info("from: {}", data);

      // destinationTable.putItem(destTx);
    }

    var list = result.items().stream().collect(Collectors.toList());

    try {
      new FileWriter("transactions.json").write(new ObjectMapper().writeValueAsString(list));
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return "Hello";
  }

  public Object populate(Object input) {

    var enhancedClient = DynamoDbEnhancedClient.builder().dynamoDbClient(this.dynamoClient).build();

    var destTableName = "TransactionDataTable-spike";
    var destinationTable =
        enhancedClient.table(destTableName, TableSchema.fromBean(AggregateData.class));

    var aggs =
        List.of(
            new AggregateData("2#1", AGGREGATE_PREFIX + "MCDONALD'S#2021#06", "MCDONALD'S", 2),
            new AggregateData("2#1", AGGREGATE_PREFIX + "COSTA#2021#06", "COSTA", 3),
            new AggregateData("2#1", AGGREGATE_PREFIX + "AMAZON#2021#06", "AMAZON", 15),
            new AggregateData("2#1", AGGREGATE_PREFIX + "SPOTIFY#2021#06", "SPOTIFY", 1),
            new AggregateData("2#1", AGGREGATE_PREFIX + "COSTA#2021#05", "COSTA", 3),
            new AggregateData("2#1", AGGREGATE_PREFIX + "AMAZON#2021#05", "AMAZON", 1),
            new AggregateData("2#1", AGGREGATE_PREFIX + "SPOTIFY#2021#05", "SPOTIFY", 1),
            new AggregateData("2#1", AGGREGATE_PREFIX + "MCDONALD'S#2021#04", "MCDONALD'S", 3),
            new AggregateData("2#1", AGGREGATE_PREFIX + "COSTA#2021#04", "COSTA", 3),
            new AggregateData("2#1", AGGREGATE_PREFIX + "AMAZON#2021#04", "AMAZON", 5),
            new AggregateData("2#1", AGGREGATE_PREFIX + "SPOTIFY#2021#04", "SPOTIFY", 1));

    for (var agg : aggs) {

      log.info("agg: {}", agg);

      destinationTable.putItem(agg);
    }
    return "Hello";
  }
}
