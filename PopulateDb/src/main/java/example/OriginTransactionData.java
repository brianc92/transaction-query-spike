package example;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey;

@DynamoDbBean
@Getter
@Setter
@ToString
public class OriginTransactionData {

  private Integer userId;
  private String merchant;
  private String transaction;
  private String transactionHash;
  private Integer day;
  private Integer month;
  private Integer year;
  private Double amount;

  @DynamoDbPartitionKey
  public String getTransactionHash() {
    return transactionHash;
  }
}
