package example;

public class Config {
  public static String DELIMINATOR = "#";

  public static String AGGREGATE_PREFIX = "agg" + DELIMINATOR;

  public static int PARTITION_MIN = 1;

  public static int PARTITION_MAX = 3;
}
