package example;

import lombok.*;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbSortKey;

@DynamoDbBean
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DestinationTransactionData {

  private String userId;
  private String date;
  private String merchant;
  private String transaction;
  private String transactionHash;
  private Integer day;
  private Integer month;
  private Integer year;
  private Double amount;

  @DynamoDbPartitionKey
  public String getUserId() {
    return userId;
  }

  @DynamoDbSortKey
  public String getDate() {
    return date;
  }

  static DestinationTransactionData of(OriginTransactionData origin) {
    var txData = new DestinationTransactionData();
    txData.setDate(
        String.format(
            "%d%s%02d%s%02d",
            origin.getYear(),
            Config.DELIMINATOR,
            origin.getMonth(),
            Config.DELIMINATOR,
            origin.getDay()));

    txData.setUserId(String.format("%d%s", origin.getUserId(), Util.generatePartitionSuffix()));
    txData.setMerchant(origin.getMerchant());
    txData.setTransaction(origin.getTransaction());
    txData.setTransactionHash(origin.getTransactionHash());
    txData.setAmount(origin.getAmount());
    txData.setDay(origin.getDay());
    txData.setMonth(origin.getMonth());
    txData.setYear(origin.getYear());

    return txData;
  }
}
